import {FlowRouter} from 'meteor/kadira:flow-router-ssr';

let timeInMillis = 1000 * 10;
FlowRouter.setPageCacheTimeout(0);

if(Meteor.isServer) {
  FlowRouter.setDeferScriptLoading(true);
}
