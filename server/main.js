import '../app/startup/server';
import '../app/startup/both';
import setupSocialAccounts from './config/socialAccounts';
import setupDefaultUsers from './loaders/users.js';

Meteor.startup(() => {
  setupDefaultUsers();
  setupSocialAccounts();
});
