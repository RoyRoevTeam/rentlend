FROM ubuntu

RUN apt-get update

RUN apt-get install locales && locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN apt-get install -y curl \
 && curl -sL https://deb.nodesource.com/setup_6.x | bash - \
 && apt-get install -y nodejs \
 && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
 && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
 && apt-get update && apt-get install yarn \
 && apt-get install -y bzip2 vim-nox-py2 htop mc git



RUN useradd -ms /bin/bash 1000
#RUN mkdir /home/1000/meteorapp
RUN chown -R 1000 /home/1000
#VOLUME . /home/1000/meteorapp

#WORKDIR /home/1000/meteorapp

USER 1000

RUN curl https://install.meteor.com/ | sh
ENV PATH "$PATH:/home/1000/.meteor"
#RUN meteor build --architecture=os.linux.x86_64 /home/1000/.meteor/packages/meteor-tool/.1.4.4_2.5g3s0q++os.linux.x86_64+web.browser+web.cordova/mt-os.linux.x86_64/dev_bundle
#CMD ["yarn", "install"]
#CMD ["yarn", "start"]

