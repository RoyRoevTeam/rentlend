import React, { Component, PropTypes }  from 'react';
import { Provider } from 'react-redux';
import configureStore from '../../../../helpers/configureStore';
import rootReducer from '../../../../site/reducers';
import LandingNavigation from './../../../../site/layout/components/LandingNavigation';
import LandingLogo from './../../../../site/layout/components/LandingLogo';
// import {TrackingCode} from '../../../../../imports/helpers/ga'

class LoginLayout extends Component {
  render() {
    let { title, subTitle, content, footer } = this.props;
    // TrackingCode()
    return (
      <Provider store={configureStore({}, rootReducer)} >
        <div className="accounts-page">
          <div className="hero" />
          <header>
            <LandingLogo />
            <LandingNavigation />

          </header>
          <div className="accounts-content">
            <h1>{title}</h1>
            <h2>{subTitle}</h2>
            <div>
              { content }
              { footer }
            </div>
          </div>
        </div>
      </Provider>
    )
  }
}

LoginLayout.PropTypes = {
  store: PropTypes.object,
  title: PropTypes.string,
  subTitle: PropTypes.string,
  content: PropTypes.func,
  footer: PropTypes.func,
};

export default LoginLayout;