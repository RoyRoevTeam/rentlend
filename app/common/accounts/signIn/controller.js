import { createContainer } from 'meteor/react-meteor-data';
import { Accounts } from 'meteor/accounts-base';
import { connect } from 'react-redux';
import SignIn from './components/SignIn'

const mapStateToProps = (state) => (
  {
    user: state.user
  }
);

const mapDispatcher = {

};

export default createContainer(() => {
  return {
    loginServices: Accounts.loginServiceConfiguration.find().fetch(),
  };
}, connect(mapStateToProps, mapDispatcher)(SignIn));