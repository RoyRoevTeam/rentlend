import React, { Component, PropTypes } from 'react'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import NoSSR from 'react-no-ssr';
import autobind from 'autobind-decorator';
import { Field, reduxForm } from 'redux-form';
import _ from 'lodash';
import cn from 'classnames';
import validate from '../../../../utils/validate';
import SignInWithService from './SignInWithService';

const renderField = ({input, label, type, meta: {touched, error, warning}}) => {
  const withError = touched && error;
  return (
    <div>
      <div>
        <input {...input} placeholder={label} type={type} className={cn({error : withError})} />
        {touched &&
        ((error && <span className="error">{error}</span>) ||
        (warning && <span className="warning">{warning}</span>))}
      </div>
    </div>
  );
};

@reduxForm({
  form: 'SignIn',
  validate: validate({
    username : ["required"],
    password : ["required"]
  })
})

export default class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errorMessage: null
    }
  }

  @autobind
  signInWithPassword(values) {
    Meteor.loginWithPassword({ username: values.username }, values.password, err => {
      if (err) {
        this.setState({ errorMessage: "Не верный логин/пароль" });
      } else {
        FlowRouter.go("home")
      }
    })
  }

  @autobind
  signInWith(service) {
    Meteor[`loginWith${_.capitalize(service)}`]({
      requestPermissions : ['email']
    }, err => {
      if (err) {
        this.setState({ errorMessage: "Не верный логин/пароль" });
      } else {
        FlowRouter.go("home")
      }
    })
  }

  @autobind
  renderError() {
    const { errorMessage } = this.state;
    if (errorMessage) {
      return (
        <ReactCSSTransitionGroup
          transitionName="alert"
          transitionAppear
          transitionAppearTimeout={200}
          transitionEnterTimeout={0}
          transitionLeaveTimeout={0}
        >
          <div className="alert alert-danger">{errorMessage}</div>
        </ReactCSSTransitionGroup>
      )
    }
  }

  render() {
    const { handleSubmit, loginServices } = this.props;
    return (
      <div className="sign-in-form">
        {this.renderError()}

        <form onSubmit={handleSubmit(this.signInWithPassword)}>
          <Field name="username" type="text" label="Ваш логин/email" component={renderField} />
          <Field name="password" type="password" label="Ваш пароль" component={renderField} />
          <button type="submit" className="btn btn-primary">
            <i className="fa fa-unlock-alt" />
            Войти
          </button>
          <NoSSR>
            <span>
              {loginServices.length > 0 ? (<div className="or"><span>Или</span></div>) : null}
              {loginServices.map((service) => {
                return (
                  <SignInWithService
                    key={service.service}
                    signInWith={this.signInWith}
                    service={service.service}
                    label={`Sign In With ${service.service}`}
                    icon={`fa-${service.service}`}
                  />
                )
              })}
            </span>
          </NoSSR>
        </form>
        <div>
          Мы не публикуем записи на вашей стене,
          не рассылаем спам вашим друзьям и не раскрываем ваши личные данные третьим лицам.
          Если у вас нет аккаунта, <a href="/signup">зарегистрируйтесь!</a>
        </div>
      </div>
    )
  }
}

SignIn.propTypes = {
  handleSubmit : PropTypes.func,
  loginServices: PropTypes.array
};
