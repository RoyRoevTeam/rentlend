import React, { Component, PropTypes } from 'react'

export default class SignInWithService extends Component {
  signIn() {
    const { signInWith, service } = this.props;
    signInWith(service);
  }
  render() {
    const { label, icon } = this.props;
    let classNames = `fa ${icon}`;
    return (
      <a onClick={this.signIn.bind(this)} className="btn">
        <i className={classNames} />
        {label}
      </a>
    )
  }
}

SignInWithService.propTypes = {
  signInWith : PropTypes.func,
  service : PropTypes.obj,
  icon : PropTypes.string,
  label : PropTypes.string,
};