import {FlowRouter} from 'meteor/kadira:flow-router-ssr'
import React from 'react'
import {mount} from 'react-mounter'
import LoginLayout from './layout/components/LoginLayout'
import SignIn  from './signIn/controller'
import SignUp from './signUp/controller'

import GuestFooter from './layout/components/GuestFooter'


FlowRouter.route("/signin", {
  name: "SignIn",
  action(){
    mount(LoginLayout, {
      content: <SignIn />,
      title: "Привет!",
      subTitle: "Просто войди в свой аккаунт",
      footer: <GuestFooter />
    })
  }
});

FlowRouter.route("/signup", {
  name: "SignUp",
  action() {
    mount(LoginLayout, {
      content: <SignUp />,
      title: "Создай учетную запись и начни продавать!",
      subTitle: "Это абсолютно бесплатно, просто попробуй.",
      footer: <GuestFooter />
    })
  }
});
