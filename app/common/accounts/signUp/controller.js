import { connect } from 'react-redux';
import { setUser } from '../../../adminPanel/actions/userActions';
import SignUp from './components/SignUp';

const mapStateToProps = (state) => (
  {
    user: state.user
  }
);

const mapDispatcher = {
  setUser
};

export default connect(mapStateToProps, mapDispatcher)(SignUp)