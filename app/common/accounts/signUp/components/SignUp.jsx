import React, { Component, PropTypes } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import autobind from 'autobind-decorator';
import { Field, reduxForm } from 'redux-form';
import cn from 'classnames';
import validate from '../../../../utils/validate';
import { Accounts } from 'meteor/accounts-base';

const renderField = ({input, label, type, meta: {touched, error, warning}}) => {
  const withError = touched && error;
  return (
    <div>
      <div>
        <input {...input} placeholder={label} type={type} className={cn({error : withError})} />
        {touched &&
        ((error && <span className="error">{error}</span>) ||
        (warning && <span className="warning">{warning}</span>))}
      </div>
    </div>
  );
};

@reduxForm({
  form: 'SignUp',
  validate: validate({
    email : ["required"],
    password : ["required"],
    confirmPassword : fields => !fields.confirmPassword ?
      "Поле обязательно для заполнения"
      : fields.confirmPassword !== fields.password ? "Пароли не совпадают" : null,
  })
})

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errorMessage : null
    }
  }

  @autobind
  signUpWithPassword(values) {
    const { setUser } = this.props;
    values.username = values.email;
    Accounts.createUser(values, (err) => {
      if (err){
        this.setState({errorMessage : err.message});
      } else {
        setUser(values);
        FlowRouter.go('home')
      }
    });
  }

  @autobind
  renderError() {
    const { errorMessage } = this.state;
    if (errorMessage) {
      return (
        <ReactCSSTransitionGroup
          transitionName="alert"
          transitionAppear
          transitionAppearTimeout={200}
          transitionEnterTimeout={0}
          transitionLeaveTimeout={0}
        >
          <div className="alert alert-danger">{errorMessage}</div>
        </ReactCSSTransitionGroup>
      )
    }
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <div className="sign-in-form">
        {this.renderError()}

        <form onSubmit={handleSubmit(this.signUpWithPassword)}>
          <Field name="email" type="text" label="Email" component={renderField} />
          <Field name="password" type="password" label="Пароль" component={renderField} />
          <Field name="confirmPassword" type="password" label="Подтвердите пароль" component={renderField} />
          <button className="btn btn-primary" type="submit" value="Зарегестрироваться">
            <i className="fa fa-unlock-alt" />
            Зарегистрироваться
          </button>
        </form>

      </div>
    )
  }
}

SignUp.propTypes = {
  setUser: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired
};

export default SignUp