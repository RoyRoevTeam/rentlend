import React, { Component, PropTypes } from 'react';
import { Field, reduxForm } from 'redux-form';
import { FormGroup, ControlLabel, FormControl, HelpBlock, Button, Input } from 'react-bootstrap';
import autobind from 'autobind-decorator';
import Cropper from 'react-cropper';
// import 'cropperjs/dist/cropper.css';
import validate from '../../utils/validate';

const isBrowser = typeof window !== 'undefined';
isBrowser ? require('cropperjs/dist/cropper.css') : undefined;

const FieldGroup = ({ id, label, help, column, input, meta, ...props }) => {
  const { touched, error, warning } = meta || {};
  return (
    <FormGroup controlId={id} validationState={touched && error && "error"}>
      <ControlLabel>{label}</ControlLabel>
      <FormControl {...props} {...input} />
      {help && <HelpBlock>{help}</HelpBlock>}
      {touched && error && <HelpBlock>{error}</HelpBlock>}
    </FormGroup>
  );
};

@reduxForm({
  form: 'addCategory',
  validate: validate({
    title : ["required"]
  })
})

export default class AddCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      src: null,
      cropResult: null,
    };
  }

  @autobind
  handleSubmit(values) {
    const { handleSubmitForm } = this.props;
    handleSubmitForm(values);
  }

  @autobind
  cropImage(e) {
    e.preventDefault();
    if (typeof this.cropper.getCroppedCanvas() === 'undefined') {
      return;
    }
    this.setState({
      cropResult: this.cropper.getCroppedCanvas().toDataURL(),
    });
  }

  @autobind
  onChangeImage(e) {
    e.preventDefault();
    let files;
    if (e.dataTransfer) {
      files = e.dataTransfer.files;
    } else if (e.target) {
      files = e.target.files;
    }
    const reader = new FileReader();
    reader.onload = () => {
      this.setState({ src: reader.result });
    };
    reader.readAsDataURL(files[0]);
  }

  render() {
    const { handleSubmit } = this.props,
      { cropResult } = this.state;
    return (
      <form onSubmit={handleSubmit(this.handleSubmit)}>
        <Field
          id="title"
          name="title"
          label="Название категории"
          placeholder="Введите название"
          component={FieldGroup}
        />
        <Field
          id="description"
          name="description"
          label="Описание категории"
          placeholder="Введите описание"
          componentClass="textarea"
          component={FieldGroup}
        />
        <div>
          <FieldGroup
            id="photo"
            name="photo"
            label="загрузите изображение"
            type="file"
            onChange={this.onChangeImage}
          />
          <Cropper
            ref={cropper => { this.cropper = cropper; }}
            style={{maxHeight: 400, width: '100%'}}
            aspectRatio={16 / 9}
            guides={false}
            src={this.state.src}
            // preview=".img-preview"
            crop={this.cropImage}
          />
          {
            !!cropResult && (
              <Button onClick={this.cropImage}>
                Обрезать фото
              </Button>
            )
          }
        </div>
        <br style={{ clear: 'both' }} />
        <Button type="submit" bsStyle="primary">
          Сохранить
        </Button>
      </form>
    );
  }
}

AddCategory.propTypes = {
  handleSubmit: PropTypes.func,
  handleSubmitForm: PropTypes.func,
};