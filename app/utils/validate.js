import _each from 'lodash/each';
import validateRules from './validateRules/index';
import validateMessages from './validateMessages';

export default function validate(formRules) {
  return (formValues) => {
    const errors = {};
    _each(formRules, (rules, field) => {
      if (typeof rules === "function")
        errors[field] = rules(formValues);
      _each(rules, (rule) => {
        const isValidRule = validateRules[rule](formValues[field]);
        if (!isValidRule) {
          errors[field] = validateMessages[rule];
        }
      });
    });

    return errors;
  };
}
