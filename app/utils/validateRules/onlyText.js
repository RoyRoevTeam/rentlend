const ONLY_TEXT_REGEX = /[^a-zA-Zа-яА-Яё]/g;

export default function (value) {
  return value && !value.match(ONLY_TEXT_REGEX);
}
