const PHONE_REGEX = /\d{10}/;
const phone = value => value && value.match(PHONE_REGEX);
export default phone;
