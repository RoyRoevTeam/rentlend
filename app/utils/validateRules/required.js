import _trim from 'lodash/trim';

export default function (value) {
  return value && !!_trim(value).length;
}
