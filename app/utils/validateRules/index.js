import isEmail from './isEmail';
import required from './required';
import onlyText from './onlyText';
import checked from './checked';
import phone from './phone';

export default {
  isEmail,
  required,
  onlyText,
  checked,
  phone,
};
