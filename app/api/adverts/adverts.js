import BaseCollection from '../BaseCollection';

const AdvertsCollection = new BaseCollection('adverts');

const AdvertsSchema = new SimpleSchema({
  title: {type : String},
  name: {type : String},
  description: {type : String},
  userId: {type : String, regEx: SimpleSchema.RegEx.Id},
  addressId: {type : String, regEx: SimpleSchema.RegEx.Id},
  categoryId: {type : String, regEx: SimpleSchema.RegEx.Id},
  // typeId: {type : String},
  // deposit: {type: Number},
  createdAt: { type : Date },
  updatedAt: {type : Date},
  isActive: {type: Boolean, defaultValue: false},
});

AdvertsCollection.schema = AdvertsSchema;

export { AdvertsCollection as default, AdvertsSchema};


