import Categories from './categories/categories';

Meteor.startup(() => {
  if (Meteor.isServer) {
    let Api = new Restivus({useDefaultAuth: true});
    Api.addCollection(Categories, {
      endpoints: {
        get: {
          authRequired: false
        },
      }
    });
  }
});