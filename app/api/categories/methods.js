import { Meteor } from 'meteor/meteor';
import Categories from './categories';

Meteor.methods({
  'categories.updateTree'(tree) {
    const allCategories = Categories.find({}).fetch();
    _.each(allCategories, category => {
      if (!_.find(tree, item => item._id === category._id))
        Categories.remove({ _id : category._id});
    });
    _.each(tree, (item, index) => {
      delete item.expanded;
      item.order = index;
      Categories.update({ _id : item._id}, item, { upsert: true })
    });
  },

  'categories.remove'(node) {
    Categories.remove(node);
  }
});