import { Mongo } from 'meteor/mongo';

const Categories = new Mongo.Collection('categories');

Categories.schema = new SimpleSchema({
  title: {type : String},
  description: {type : String},
  order: {type: Number, defaultValue: 0},
  isActive: {type: Boolean, defaultValue: true},
  children: {type : [Object]}
});

if (Meteor.isServer) {
  Meteor.publish('categories', function tasksPublication() {
    return Categories.find();
  });
}

export default Categories;