import { Mongo } from 'meteor/mongo';

const Addresses = new Mongo.Collection('addresses');

Addresses.schema = new SimpleSchema({
  clientId: {type : String, regEx: SimpleSchema.RegEx.Id},
  name: {type : String},
  city: {type : String},
  region: {type : String},
  street: {type : String},
  house: {type : String},
  korp: {type : String},
  room: {type : String},
  flat: {type : String},
  coords: {type : Object},
  createdAt: {type : Date},
  updatedAt: {type : Date},
  isActive: {type: Boolean, defaultValue: false},
});