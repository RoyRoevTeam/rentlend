import { Mongo } from 'meteor/mongo';

const Prices = new Mongo.Collection('prices');

Prices.schema = new SimpleSchema({
  value: {type : Number},
  quantity: {type : String},
  quantityType: {type : String},
  advertId: {type : String, regEx: SimpleSchema.RegEx.Id},
  createdAt: {type : Date},
  updatedAt: {type : Date},
  isActive: {type: Boolean, defaultValue: false},
});