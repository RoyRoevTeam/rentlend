import { Mongo } from 'meteor/mongo';

export default class BaseCollection extends Mongo.Collection {
  insert(doc, callback) {
    doc.createdAt = doc.createdAt || new Date();
    doc.updatedAt = doc.updatedAt || new Date();
    return super.insert(doc, callback);
  }
  update(doc, callback) {
    doc.updatedAt = doc.updatedAt || new Date();
    return super.update(doc, callback);
  }
}
