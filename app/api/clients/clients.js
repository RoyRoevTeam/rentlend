import { Mongo } from 'meteor/mongo';

const Clients = new Mongo.Collection('clients');

Clients.schema = new SimpleSchema({
  name: {type : String},
  email: {type : String},
  phone: {type : String},
  createdAt: {type : Date},
  updatedAt: {type : Date},
  isActive: {type: Boolean, defaultValue: false},
});