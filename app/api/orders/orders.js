import { Mongo } from 'meteor/mongo';

const Orders = new Mongo.Collection('orders');

Orders.schema = new SimpleSchema({
  advertId: {type : String, regEx: SimpleSchema.RegEx.Id},
  clientId: {type : String, regEx: SimpleSchema.RegEx.Id},
  dateFrom: {type : String},
  dateTo: {type : String},
  priceId: {type : String, regEx: SimpleSchema.RegEx.Id},
  createdAt: {type : Date},
  updatedAt: {type : Date},
});