import React, { Component, PropTypes } from 'react';
import { Provider } from 'react-redux';
import NoSSR from 'react-no-ssr';
import Navigation from './Navigation';
import rootReducer from '../../reducers';
import configureStore from '../../../helpers/configureStore';

export default class App extends Component {
  render() {
    let { content } = this.props;
    return (
      <Provider store={configureStore({}, rootReducer)}>
        <div className="container">
          <header>
            <Navigation />
          </header>
          <NoSSR>
            {content}
          </NoSSR>
        </div>
      </Provider>
    );
  }
}

App.propTypes = {
  content: PropTypes.object,
};
