import React, { Component } from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
// import Link from '../Link';

export default class Navigation extends Component {
  render() {
    return (
      <Navbar>
        <Navbar.Header>
          <Navbar.Brand>
            <a href="/">RENTLEND - CMS</a>
          </Navbar.Brand>
        </Navbar.Header>
        <Nav>
          <NavItem eventKey={1} href="/app/categories">Категории</NavItem>
          <NavItem eventKey={2} href="/app/adverts">Заявки</NavItem>
        </Nav>
      </Navbar>
    )
  }
}