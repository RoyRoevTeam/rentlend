import React from 'react';
import { compose } from 'react-komposer';
import AppLayout from  './components/AppLayout'

const composition = (props, onData) => {
  // console.log(Roles.userIsInRole(Meteor.user(), 'admin', Roles.GLOBAL_GROUP));
  if (!Meteor.loggingIn() && !(Meteor.user() && Roles.userIsInRole(Meteor.user(), 'admin', Roles.GLOBAL_GROUP))) {
    setTimeout(() => { FlowRouter.go('/')})
  }else{
    onData(null, props)
  }
};

export default compose(composition)(AppLayout)
