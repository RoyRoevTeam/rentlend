import { SET_USER } from '../constants/index';

export const setUserUnsafe = (user) => {
  return {
    type: SET_USER,
    user
  }
};

export const setUser = (user) => {
  return (dispatch) => {
    dispatch(setUserUnsafe(user))
  }
};