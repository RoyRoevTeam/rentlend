import React from 'react'
import { mount } from 'react-mounter';
import AppLayout from './layout/controller';
import Adverts from './adverts/components/Adverts';

const Categories = Meteor.isClient ? require('./categories/Categories').default : null;

const adminRoutes = FlowRouter.group({
  prefix: '/app',
  name: 'admin'
});

adminRoutes.route("/", {
  name: "App",
  action() {
    mount(AppLayout, {content : null})
  }
});

adminRoutes.route("/categories", {
  name: "Categories",
  action() {
    mount(AppLayout, {content : <Categories />})
  }
});

adminRoutes.route("/adverts", {
  name: "Adverts",
  action() {
    mount(AppLayout, {content : <Adverts />})
  }
});
