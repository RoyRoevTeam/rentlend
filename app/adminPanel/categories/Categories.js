import React, { Component, PropTypes } from 'react';
import { Meteor } from 'meteor/meteor';
import autobind from 'autobind-decorator';
import { Modal } from 'react-bootstrap';
import { createContainer } from 'meteor/react-meteor-data';
import AddCategory from '../../common/forms/AddCategory';
import CategoriesApi from '../../api/categories/categories';

const maxDepth = 5;

class Categories extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchString: '',
      searchFocusIndex: 0,
      searchFoundCount: null,
      openAddModal: false,
      selectNode: null,
      treeData: props.categories || [],
    };
  }

  componentWillReceiveProps(np) {
    if (!!np.categories.length && this.state.treeData.length === 0)
      this.setState({treeData : np.categories})
  }

  @autobind
  updateTreeData(treeData) {
    this.setState({ treeData });
  }

  @autobind
  expand(expanded) {
    const toggleExpandedForAll = require('react-sortable-tree').toggleExpandedForAll;
    this.setState({
      treeData: toggleExpandedForAll({
        treeData: this.state.treeData,
        expanded,
      }),
    });
  }

  @autobind
  expandAll() {
    this.expand(true);
  }

  @autobind
  collapseAll() {
    this.expand(false);
  }

  @autobind
  openAddModal() {
    this.setState({ openAddModal: true });
  }

  @autobind
  handleSubmitForm(values) {
    CategoriesApi.insert(values, () => {
      this.setState({ openAddModal: false }, () => {
        const { categories } = this.props;
        this.setState({ treeData : categories });
      });
    });
  }

  @autobind
  moveNode({node, treeIndex, path, treeData}) {
    console.debug( // eslint-disable-line no-console
      'node:', node,
      'treeIndex:', treeIndex,
      'path:', path,
    );
    Meteor.call('categories.updateTree', treeData, (err, result) => {
      console.log(result);
    });
  }

  @autobind
  removeNode({node, path}) {
    if (confirm("Вы хотите удалить категорию?")) {
      Meteor.call('categories.remove', node, () => {
        const { categories } = this.props;
        this.setState({ treeData : categories });
      });
    }
  }

  @autobind
  editNode({node}) {
    this.setState({ openAddModal: true, selectNode: node })
  }

  render() {
    const {
      treeData,
      searchString,
      searchFocusIndex,
      searchFoundCount,
      openAddModal,
      selectNode,
    } = this.state;

    const selectPrevMatch = () => this.setState({
      searchFocusIndex: searchFocusIndex !== null ?
        (((searchFoundCount + searchFocusIndex) - 1) % searchFoundCount) :
        searchFoundCount - 1,
    });

    const selectNextMatch = () => this.setState({
      searchFocusIndex: searchFocusIndex !== null ?
        ((searchFocusIndex + 1) % searchFoundCount) :
        0,
    });

    const isVirtualized = true;
    const treeContainerStyle = isVirtualized ? { height: 600 } : {};

    const SortableTree = require('react-sortable-tree').default;

    return (
      <div>
        <section className='main-content'>
          <h3>Категории</h3>

          <button onClick={this.openAddModal}>
            Добавить категорию
          </button>

          <button onClick={this.expandAll}>
            Показать все
          </button>

          <button onClick={this.collapseAll}>
            Скрыть все
          </button>

          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <form
            style={{ display: 'inline-block' }}
            onSubmit={(event) => {
              event.preventDefault();
            }}
          >
            <label htmlFor="find-box">
              Поиск:&nbsp;

              <input
                id="find-box"
                type="text"
                value={searchString}
                onChange={event => this.setState({ searchString: event.target.value })}
              />
            </label>

            <button
              type="button"
              disabled={!searchFoundCount}
              onClick={selectPrevMatch}
            >
              &lt;
            </button>

            <button
              type="submit"
              disabled={!searchFoundCount}
              onClick={selectNextMatch}
            >
              &gt;
            </button>

            <span>
              &nbsp;
              {searchFoundCount > 0 ? (searchFocusIndex + 1) : 0}
              &nbsp;/&nbsp;
              {searchFoundCount || 0}
            </span>
          </form>

          <div style={treeContainerStyle}>
            <SortableTree
              treeData={treeData}
              onChange={this.updateTreeData}
              onMoveNode={this.moveNode}
              maxDepth={maxDepth}
              searchQuery={searchString}
              searchFocusOffset={searchFocusIndex}
              searchFinishCallback={matches =>
                this.setState({
                  searchFoundCount: matches.length,
                  searchFocusIndex: matches.length > 0 ? searchFocusIndex % matches.length : 0,
                })
              }
              isVirtualized={isVirtualized}
              generateNodeProps={rowInfo => ({
                buttons: [
                  <button
                    style={{
                      verticalAlign: 'middle',
                    }}
                    onClick={() => this.editNode(rowInfo)}
                  >
                    ℹ
                  </button>,
                  <button
                    style={{
                      verticalAlign: 'middle',
                    }}
                    onClick={() => this.removeNode(rowInfo)}
                  >
                    x
                  </button>,
                ],
              })}
            />
          </div>
        </section>
        <Modal show={openAddModal} onHide={() => this.setState({ openAddModal: false })}>
          <Modal.Header closeButton>
            <Modal.Title>Добавление категории</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <AddCategory handleSubmitForm={this.handleSubmitForm} data={selectNode} />
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

Categories.propTypes = {
  categories: PropTypes.array.isRequired,
};

export default createContainer(() => {
  Meteor.subscribe('categories');

  return {
    categories: CategoriesApi.find({}, { sort: { order: 1 } }).fetch(),
  }
}, Categories);