import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import runtime from './runtime';
import user from './user';

export default combineReducers({
  runtime,
  user,
  form: formReducer,
});
