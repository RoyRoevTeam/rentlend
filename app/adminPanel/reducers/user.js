import { SET_USER } from '../constants/index';

const user = (state = null, action) => {
  switch (action.type) {
    case SET_USER:
      state = {...action.user};
      return state;
    default:
      return state;
  }
};

export default user;
