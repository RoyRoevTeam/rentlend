import React from 'react';
import { Provider } from 'react-redux';
import LandingLogo from "./LandingLogo";
import LandingNavigation from "../controllers/LandingNavigation"
import rootReducer from '../../reducers';
import configureStore from '../../../helpers/configureStore';
// import {TrackingCode} from '../../../../imports/helpers/ga'

class LandingLayout extends React.Component {
  render() {
    // TrackingCode()
    return (
      <Provider store={configureStore({}, rootReducer)}>
        <div className="landing-page">
          <div className="hero" />
          <header>
            <LandingLogo />
            <LandingNavigation />
          </header>
          { this.props.content }
        </div>
      </Provider>
    )
  }
}

export default LandingLayout