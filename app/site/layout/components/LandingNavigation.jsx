import React, { Component, PropTypes } from 'react';

export default class LandingNavigation extends Component {
  activeClass(routeName) {
    return FlowRouter.getRouteName() === routeName ? "active" : ""
  }

  logout() {
    Meteor.logout((err) => {
      FlowRouter.go("home")
    })
  }

  adminNavigation () {
    if (Roles.userIsInRole(this.props.user, 'admin', Roles.GLOBAL_GROUP)) {
      return (
        <li >
          <a href="/app/">Admin<span className="line" /></a>
        </li>
      );
    }
    return null;
  }

  signOut(event) {
    if (event && event.preventDefault) {
      event.preventDefault()
    }

    this.logout();
    return false;
  }

  render() {
    const { user } = this.props;
    return (
      <nav className="nav-primary">
        <ul className="clearfix">
          <li className={this.activeClass('Features')}>
            <a href="/about">О нас<span className="line" /></a>
          </li>
          <li className={this.activeClass('Elements')}>
            <a href="/advert">Подать объявление<span className="line" /></a>
          </li>
          {this.adminNavigation()}
          {user ? (
            <li key="SignOut" className={this.activeClass('SignOut')}>
              <a onClick={this.signOut.bind(this)} href="/">
                Выйти ({user.profile.name})<span className="line" />
              </a>
            </li>
          ) : (
            <li className={this.activeClass('SignIn')}>
              <a href="/signin">Войти<span className="line" /></a>
            </li>
          )}
        </ul>
      </nav>
    )
  }
}

LandingNavigation.propTypes = {
  user: PropTypes.object,
};
