import React from 'react';
import {mount} from 'react-mounter';
import LandingLayout from './components/LandingLayout';
import FourOhFour from './components/NotFound';

FlowRouter.notFound = {
  action(){
    mount(LandingLayout, {content: <FourOhFour />})
  }
};