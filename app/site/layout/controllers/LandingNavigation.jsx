import React from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import LandingNavigation from '../components/LandingNavigation'

export default createContainer(() => {
  return {
    user: Meteor.user(),
    activeRoute: FlowRouter.getRouteName()
  };
}, LandingNavigation);