import React, { Component, PropTypes } from 'react';
import { CSSTransitionGroup } from 'react-transition-group';
import transitions from './transitions';

class ReactTransitions extends Component {
  render() {
    const {
      children,
      transition,
      ...restProps
    } = this.props;

    const {
      leave, leaveActive,
      enter, enterActive,
      leaveTimeout, enterTimeout
    } = transitions.find( t => transition === t.name );

    const childComponents = React.Children.map( children, ( child, index ) => (
      <div key={index}>{child}</div>
    ));

    return (
      <CSSTransitionGroup
        component="div"
        style={{position: 'relative'}}
        transitionName={{
          leave,
          leaveActive: leaveActive || '',
          enter,
          enterActive: enterActive || ''
        }}
        transitionLeaveTimeout={leaveTimeout}
        transitionEnterTimeout={enterTimeout}
        {...restProps}
      >
        {childComponents}
      </CSSTransitionGroup>
    );
  }
}

ReactTransitions.propTypes = {
  children: ( props, propName, componentName ) => {
    if (props[ propName ] === null) {
      return null;
    }

    if ( React.Children.count( props[ propName ] ) > 1 ) {
      return new Error(
        `Invalid \`${propName}\` supplied to ` +
        `\`${componentName}\`, expected a single ReactElement.`
      );
    }

    const child = React.Children.only( props[ propName ] );

    if ( !React.isValidElement( child ) ) {
      return new Error(
        `Child \`${propName}\` in ` +
        `\`${componentName}\` is not a valid React element.`
      );
    }

    if ( !child.key ) {
      return new Error(
        `Key is not provided to \`${propName}\` in ` +
        `\`${componentName}\`.`
      );
    }

    return null;
  },
  width: PropTypes.oneOfType( [ PropTypes.number, PropTypes.string ] ).isRequired,
  height: PropTypes.oneOfType( [ PropTypes.number, PropTypes.string ] ).isRequired,
  transition: PropTypes.string,
};

export default ReactTransitions;
