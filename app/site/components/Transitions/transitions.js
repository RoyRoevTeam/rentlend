const Transitions = [
  {
    name: 'move-to-left-easing-move-from-right',
    leave: 'moveToLeftEasing',
    leaveActive: 'react-transitions-top',
    enter: 'moveFromRight',
    leaveTimeout: 700,
    enterTimeout: 600
  },
  {
    name: 'move-to-right-easing-move-from-left',
    leave: 'moveToRight',
    leaveActive: 'react-transitions-top',
    enter: 'moveFromLeftEasing',
    leaveTimeout: 700,
    enterTimeout: 600
  }
];

export default Transitions;
