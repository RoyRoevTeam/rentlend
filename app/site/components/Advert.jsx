import React, { Component, PropTypes } from 'react';
import autobind from 'autobind-decorator';
import { DocHead } from 'meteor/kadira:dochead';
import { createContainer } from 'meteor/react-meteor-data';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import cn from 'classnames';
import Select from 'react-select';
import { changeStep } from '../actions/advertActions';
import ReactTransitions from './Transitions/index';
import CategoriesApi from '../../api/categories/categories';
import AdvertsCollection, { AdvertsSchema } from '../../api/adverts/adverts';

const InputField = ({ input, label, name, placeholder }) => {
  return (
    <div className="form-group">
      <label htmlFor={name} className="col-sm-2 control-label">{label}</label>
      <div className="col-sm-10">
        <input className="form-control" id={name} {...input} placeholder={placeholder} />
      </div>
    </div>
  );
};

const SelectField = ({ input, label, name, options, value }) => {
  return (
    <div className="form-group">
      <label htmlFor={name} className="col-sm-2 control-label">{label}</label>
      <div className="col-sm-10">
        <Select
          id="name"
          name={name}
          options={options}
          value={input.value || ''}
          {...input}
          onBlur={() => input.onBlur(input.value)}
          onChange={value => input.onChange(value)}
          simpleValue
        />
      </div>
    </div>
  );
};

const TextField = ({ input, label, name, placeholder }) => {
  return (
    <div className="form-group">
      <label htmlFor={name} className="col-sm-2 control-label">{label}</label>
      <div className="col-sm-10">
        <textarea className="form-control" id={name} {...input} placeholder={placeholder} />
      </div>
    </div>
  );
};

const FormContainer = ({ title, desc, children }) => (
  <div className="advert-body">
    <div className="title">{title}</div>
    <div className="description">{desc}</div>
    <div className="form-horizontal">
      {children}
    </div>
  </div>
);

@reduxForm({
  form: 'advertForm',
})

class Advert extends Component {
  constructor(props) {
    super(props);
    this.state = {
      transition: 'move-to-left-easing-move-from-right',
      steps: [
        { title: 'Общая информация о товаре' },
        { title: 'Медиа' },
        { title: 'Ценовая политика' },
      ]
    }
  }

  componentWillReceiveProps({ step: nextStep }) {
    const { step } = this.props;
    if (nextStep !== step) {
      this.setState({
        transition:  nextStep > step ? 'move-to-left-easing-move-from-right' : 'move-to-right-easing-move-from-left'
      })
    }
  }

  @autobind
  handleSubmit(values) {
    console.log(values);
    const { reset, user } = this.props;
    const data = {
      ...values,
      categoryId: values.category,
      userId: user._id,
    };
    AdvertsCollection.insert(AdvertsSchema.clean(data), error => {
      if (!error) reset();
    })
  }

  @autobind
  formStep() {
    const { categories = [], step = 1, changeStep } = this.props;
    switch (step) {
      default: return null;
      case 1: return (
        <FormContainer title="Шаг первый" desc="Общая информация о вашем товаре" key={1}>
          <fieldset className={cn({show: step === 1})}>
            <Field
              component={InputField}
              name="name"
              label="Название"
              placeholder="Введите название"
            />
            <br />
            <Field
              component={SelectField}
              label="Категория"
              name="category"
              type="select"
              options={categories}
              multi
            />
            <br />
            <Field
              component={TextField}
              label="Описание"
              name="description"
              placeholder="Введите описание продукта"
            />
            <div className="text-center">
              <div className="button" onClick={() => changeStep(step + 1)}>Далее</div>
            </div>
          </fieldset>
        </FormContainer>
      );
      case 2: return (
        <FormContainer title="Шаг второй" desc="Информация, что бы продвинуть ваш товар" key={2}>
          <fieldset className={cn({show: step === 2})}>
            <Field
              component={InputField}
              name="name"
              label="Название"
              placeholder="Введите название"
            />
            <br />
            <Field
              component={SelectField}
              label="Категория"
              name="category"
              type="select"
              options={categories}
            />
            <br />
            <Field
              component={TextField}
              label="Описание"
              name="description"
              placeholder="Введите описание продукта"
            />
            <div className="text-center">
              <div className="button prev" onClick={() => changeStep(step - 1)}>Назад</div>
              <div className="button next" onClick={() => changeStep(step + 1)}>Далее</div>
            </div>
          </fieldset>
        </FormContainer>
      );
      case 3: return (
        <FormContainer title="Шаг третий" desc="Информация о стоимости товара" key={3}>
          <fieldset className={cn({show: step === 3})}>
            <Field
              component={InputField}
              name="name"
              label="Название"
              placeholder="Введите название"
            />
            <br />
            <Field
              component={SelectField}
              label="Категория"
              name="category"
              type="select"
              options={categories}
            />
            <br />
            <Field
              component={TextField}
              label="Описание"
              name="description"
              placeholder="Введите описание продукта"
            />
            <div className="text-center">
              <div className="button prev" onClick={() => changeStep(step - 1)}>Назад</div>
              <button className="button submit" type="submit">Отравить</button>
            </div>
          </fieldset>
        </FormContainer>
      );
    }
  }

  render() {
    const { transition, steps = [] } = this.state;
    const { step = 1, user, handleSubmit } = this.props;
    const metaInfo = { name: "description", content: "" };
    DocHead.addMeta(metaInfo);
    DocHead.setTitle("Заявка на подачу объявления");
    return (
      <div className="advert">
        {
          !user ? (
            <div className="need-auth">
              <div className="msg">
                Чтобы разместить свое объявление, вы должны зарегистрироваться.
              </div>
              <a href="/signin" className="button">Зарегистрироваться</a>
            </div>
          ) : (
            <div className="advert-form">
              <ul className="advert-progress-bar">
                {steps.map(({ title }, key) => (
                  <li className={cn({ active: key + 1 <= step })}>{title}</li>
                ))}
              </ul>
              <form onSubmit={handleSubmit(this.handleSubmit)}>
                <ReactTransitions
                  width={600}
                  height={400}
                  transition={transition}
                >
                  { this.formStep() }
                </ReactTransitions>
              </form>
            </div>
          )
        }

      </div>
    )
  }
}

Advert.propTypes = {
  categories: PropTypes.array,
  step: PropTypes.number,
  changeStep: PropTypes.func,
  user: PropTypes.object,
  reset: PropTypes.func,
};

const mapStateToProps = (state) => (
  {
    step: state.advertReducer.step
  }
);

export default createContainer(() => {
  Meteor.subscribe('categories');

  return {
    user: Meteor.user(),
    categories: CategoriesApi.find({}, { sort: { order: 1 } }).fetch()
      .map(({_id, title}) => ({ value: _id, label: title })),
  }
}, connect(mapStateToProps, { changeStep })(Advert));
