import React, { Component } from 'react';
import {DocHead} from 'meteor/kadira:dochead';
import ReactSVG from 'react-svg';

const metaInfo = {
  name: "description",
  content: ""
};

class About extends Component {
  render() {
    DocHead.addMeta(metaInfo);
    DocHead.setTitle("RENTLEND - вещи в аренду | О нас");
    return (
      <div className="about">
        <h1>RENTLEND найдет применение вашим вещам</h1>
        <ul>
          <li>
            <ReactSVG className="icon" path="/icons/smile.svg" />
            <span className="text">
              Мы позиционируем себя, как площадка, которая помогает одним людям заработать, на том, что их вещи сдаются
              в аренду, а другим - воспользоваться этими вещами для пробы или для временной необходимости.
            </span>
          </li>
          <li>
            <ReactSVG className="icon" path="/icons/smile.svg" />
            <span className="text">
              Мы не берем проценты от ваших сделок, мы помогаем найти вам друг друга!
            </span>
          </li>
          <li>
            <ReactSVG className="icon" path="/icons/smile.svg" />
            <span className="text">
              Мы будем совершенствоваться со временем и хотим предоставить вам удобный сервис, удовлетворяющий вашим
            потребностям.
            </span>
          </li>
        </ul>
      </div>
    )
  }
}

export default About
