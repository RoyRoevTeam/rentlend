import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { random } from 'lodash';
import CategoriesApi from '../../../api/categories/categories';

const cats = ['animals', 'architecture', 'nature', 'people', 'tech'];

class CategoriesList extends Component {
  render() {
    const { categories = [] } = this.props;
    return (
      <div className="categories-list">
        <h3>Популярные категории</h3>
        <div className="items">
          {categories.map(({ title, name }) => (
            <a
              className="item"
              href={`/categories/${name}`}
              style={{ backgroundImage: `url(${`http://placeimg.com/800/800/${cats[random(0, 4)]}`})`}}
            >
              <div className="item-bg">
                <div className="title">{title}</div>
              </div>
            </a>
          ))}
        </div>

      </div>
    )
  }
}

CategoriesList.propTypes = {
  categories: PropTypes.array,
};

/*export default createContainer(() => {
  Meteor.subscribe('categories');

  return {
    categories: CategoriesApi.find({}, { sort: { order: 1 } }).fetch(),
  }
}, CategoriesList);*/

export default CategoriesList;
