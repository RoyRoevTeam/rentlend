import { compose } from 'react-komposer';
import { Tracker } from 'meteor/tracker';
import Home from './Home';
import CategoriesApi from '../../../api/categories/categories';


function getTrackerLoader(reactiveMapper) {
  return (props, onData, env) => {
    let trackerCleanup = null;
    const handler = Tracker.nonreactive(() => {
      return Tracker.autorun(() => {
        // assign the custom clean-up function.
        trackerCleanup = reactiveMapper(props, onData, env);
      });
    });

    return () => {
      if(typeof trackerCleanup === 'function') trackerCleanup();
      return handler.stop();
    };
  };
}

const composer = (props, onData) => {
  if (Meteor.subscribe('categories').ready()) {
    const categories = CategoriesApi.find({}, { sort: { order: 1 } }).fetch();
    onData(null, { categories });
  }
  // Meteor.subscribe('categories');
};

export default compose(getTrackerLoader(composer))(Home);
