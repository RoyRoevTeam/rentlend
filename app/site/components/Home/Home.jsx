import React, { Component, PropTypes } from 'react';
import {DocHead} from 'meteor/kadira:dochead';
import CategoriesList from '../Categories';

const metaInfo = {
  name: "description",
  content: "На нашей площадке собраны все вещи в Москве, которые сдают в аренду"
};

class Home extends Component {
  render() {
    const { categories = [] } = this.props;
    DocHead.addMeta(metaInfo);
    DocHead.setTitle("RENTLEND - вещи в аренду | Главная");
    return (
      <div className="home">
        <div className="cta with-bg">
          <h1>Начни зарабатывать уже сегодня</h1>
          <h2>Просто сдай свои вещи в аренду</h2>
          <a className="btn btn-primary" href="/advert">Начать</a>
          <div className="bg" />
        </div>
        <CategoriesList categories={categories} />
        <div className="landing-row">
          <div className="welcome-promo with-bg">
            <div className="welcome-promo-content">
              <h2>Начни свой малый бизнес уже сегодня</h2>
              <p>На нашей площадке люди хотят взять нужные им вещи у вас в аренду.</p>
              <a className="btn btn-primary" href="/features">Начать</a>
            </div>
            <div className="bg" />
          </div>
        </div>
      </div>
    )
  }
}

export default Home;
