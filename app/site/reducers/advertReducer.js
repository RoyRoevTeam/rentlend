import  { CHANGE_STEP } from '../constants/actionTypes';

const initialState = {
  step: 1
};

const advertReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_STEP:
      return {...state, step: action.step };
    default:
      return state;
  }
};

export default advertReducer;
