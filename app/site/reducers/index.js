import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import advertReducer from './advertReducer';

export default combineReducers({
  form: formReducer,
  advertReducer,
});
