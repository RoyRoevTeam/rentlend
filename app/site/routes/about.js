import React from 'react'
import {mount} from 'react-mounter'
import About from '../components/About'
import LandingLayout from '../layout/components/LandingLayout'

FlowRouter.route("/about", {
  name: "about",
  action() {
    mount(LandingLayout, {content: <About />})
  }
});
