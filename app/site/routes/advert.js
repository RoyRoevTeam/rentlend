import React from 'react'
import {mount} from 'react-mounter'
import Advert from '../components/Advert'
import LandingLayout from '../layout/components/LandingLayout'

FlowRouter.route("/advert", {
  name: "advert",
  action() {
    mount(LandingLayout, {content: <Advert />})
  }
});
