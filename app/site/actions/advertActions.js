/* eslint-disable import/prefer-default-export */

import { CHANGE_STEP } from '../constants/actionTypes';

export const changeStep = (step) => (
  {
    type: CHANGE_STEP,
    step,
  }
);
