import { Meteor } from 'meteor/meteor';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';
import createLogger from './logger/index';

const __DEV__ = Meteor.isDevelopment;

export default function configureStore(initialState, rootReducer) {
  const middleware = [thunk];
  let enhancer;
  if (__DEV__) {
    middleware.push(createLogger());
    // https://github.com/zalmoxisus/redux-devtools-extension#redux-devtools-extension
    let devToolsExtension = f => f;
    if (Meteor.isClient && window.devToolsExtension) {
      devToolsExtension = window.devToolsExtension();
    }
    enhancer = compose(
      applyMiddleware(...middleware),
      devToolsExtension,
    );
  } else {
    enhancer = applyMiddleware(...middleware);
  }

  const store = createStore(rootReducer, initialState, enhancer);

  if (__DEV__ && module.hot) {
    // module.hot.accept('../reducers', () =>
      // eslint-disable-next-line global-require
      // store.replaceReducer(require('../reducers').default),
    // );
  }

  return store;
}
