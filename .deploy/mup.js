module.exports = {
  servers: {
    one: {
      // TODO: set host address, username, and authentication method
      host: '185.159.130.236',
      username: 'root',
      pem: '/Users/user/.ssh/id_rsa',
      // password: 'server-password'
      // or neither for authenticate from ssh-agent
    }
  },

  meteor: {
    // TODO: change app name and path
    name: 'rentlend',
    path: '../',

    servers: {
      one: {
        host: '185.159.130.236',
        username: 'root',
        pem: '/Users/user/.ssh/id_rsa',
      },
    },

    buildOptions: {
      // serverOnly: true,
    },

    env: {
      // TODO: Change to your app's url
      // If you are using ssl, it needs to start with https://
      ROOT_URL: 'http://rentlend.ru',
      PORT: 8000,
      MONGO_URL: 'mongodb://localhost/rentlend',
    },

    docker: {
      // change to 'kadirahq/meteord' if your app is not using Meteor 1.4
      image: 'abernix/meteord:base',
      // bind: '127.0.0.1',
      // imagePort: 81, // (default: 80, some images EXPOSE different ports)
    },

    // This is the maximum time in seconds it will wait
    // for your app to start
    // Add 30 seconds if the server has 512mb of ram
    // And 30 more if you have binary npm dependencies.
    deployCheckWaitTime: 60,

    // Show progress bar while uploading bundle to server
    // You might need to disable it on CI servers
    enableUploadProgressBar: true
  },

  mongo: {
    port: 27017,
    version: '3.4.1',
    servers: {
      one: {
        host: '188.225.74.233',
        username: 'root',
        pem: '/Users/user/.ssh/id_rsa',
      }
    }
  },

  proxy: {
    domains: 'rentlend.ru,www.rentlend.ru'
  }
};
