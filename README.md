##Versions

- 0.0.3 - added schemas, add react-cropped, image upload, add categories remove
- 0.0.2 - categories tree data, categories schema
- 0.0.1 - starter kit with meteor and React.js 

### Starting app

```
yarn install
meteor
```